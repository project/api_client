# decoupled-router-client-example

## 0.0.4

### Patch Changes

- Updated dependencies [1f0bed1]
  - @drupal-api-client/graphql-client@0.1.3

## 0.0.3

### Patch Changes

- @drupal-api-client/graphql-client@0.1.2

## 0.0.2

### Patch Changes

- @drupal-api-client/graphql-client@0.1.1

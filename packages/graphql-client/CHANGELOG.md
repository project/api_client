# @drupal-api-client/graphql-client

## 0.1.3

### Patch Changes

- 1f0bed1: Publish pre-release version of @drupal-api-client/graphql-client

## 0.1.2

### Patch Changes

- Updated dependencies [6225317]
  - @drupal-api-client/api-client@1.3.0

## 0.1.1

### Patch Changes

- Updated dependencies [1830ea4]
  - @drupal-api-client/api-client@1.1.0
